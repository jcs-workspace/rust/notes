/**
 * $File: notes.rs $
 * $Date: 2024-04-01 00:47:15 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2024 by Shen, Jen-Chieh $
 */
use std::env;
use rand::Rng;

#[allow(dead_code)]
fn rand_example() {
    let num = rand::thread_rng().gen_range(0..100);
    println!("char: {}", num);
}

#[allow(dead_code)]
fn args() {
    let args: Vec<String> = env::args().collect();
    for arg in args.clone() {
        println!("arg: {}", arg);
    }
    println!("2th: {}", args[1]);
}
